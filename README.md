# solebull.gitlab.io

[[_TOC_]]

A new homepage based on *solebull.fr* and using `nanoc` static website 
generator.

## Dependencies

To install needed dependencies (at least on Debian 11 buulseye) :

	bundle install

## Building and viewing

	make

then `make view` and open `http://127.0.0.1:3000/`.


## Unit tests

	make check

## Troubleshooting

*kramdown* gem is know to cause the following error when used with ruby 3.0 :
`cannot load such file -- kramdown`. Please use `bundle` or `rvm` to switch to
a supported ruby version.
