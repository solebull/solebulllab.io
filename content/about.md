---
title: "solebull - streamer PC gaming+dev"
menu: apropos
---

Vous en voulez plus ? Voici mon setup t comment me contacter :

## Setup

Streaming *Playstation*

  * PS4 Pro 1To;
  * Playstation Camera v2.

Pour le *retro-gaming*

  * Quad-Core AMD 4100 3.6Ghz;
  * 10 Go RAM;
  * 1 To HD;
  * Nvidia GeForce GTX 260;
  * OS : Debian GNU/Linux Buster;
  * Streaming : OBS 21.1.2;
  * Emulateur Atari : Hatari 2.0.0;

Setup commun :

  * Micro Auna MIC-900S USB;

## Contact

Pour nous contacter en privé :

  * Twitter : [@solebull42](https://twitter.com/Solebull42);
  * Facebook : [solebull42](https://www.facebook.com/solebull42/)
  * Mail : [solebull42&lt;at&gt;gmail&lt;dot&gt;com](mailto:solebull42@gmail.com)
