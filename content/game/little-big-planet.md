---
title: Little Big Planet 3
playlist: PLk_fUHaG1xodc-zQbo8klv-UJ1N0QZufz
layout: /game.*
menu: games
---

## Little Big Planet 3 ##

*Little Big Planet 3* — abrégé en LBP3 — est un jeu vidéo de plates-formes 
développé par Sumo Digital et édité par Sony Computer Entertainment, 
sorti en novembre 2014 sur PlayStation 3 et PlayStation 4.

Notez que depuis que nous jouons aux niveaux communautaires, nous streamons
*LBP* exclusivement chez [twitch](https://www.twitch.tv/solebull).
