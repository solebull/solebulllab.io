---
title: "Blacklight: Retribution"
playlist: 
layout: /game.*
menu: games
---

## Blacklight: Retribution ##

*Blacklight: Retribution* est un FPS gratuit dans lequel vous combattez 
vos ennemis dans un univers futuriste. Vous possédez un exosquelette armé 
et disposez d'une artillerie d'armes de poing plutôt lourde. Il est sorti
sur PS4 le  3 april 2012.
