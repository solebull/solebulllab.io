---
title:  Trove
playlist: 
layout: /game.*
menu: games
---

## Trove ##

Trove est un jeu de sandbox voxel développé et publié par Trion Worlds. 
Le jeu est sorti sur PlayStation 4 le 14 mars 2017.
