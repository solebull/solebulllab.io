---
title: "Dwarf fortress"
playlist: 
layout: /game.*
menu: games
---

## Slaves to Armok II: Dwarf Fortress

*Slaves to Armok II: Dwarf Fortress* est un jeu vidéo indépendant a la fois 
de gestion et rogue-like. Il est développé par deux frères américains, 
**Tarn** et **Zach Adams**.

Voir plus sur le [wikipedia annglais](https://en.wikipedia.org/wiki/Dwarf_Fortress).
