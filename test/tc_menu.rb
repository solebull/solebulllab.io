require "test/unit"

require_relative "../lib/menu.rb"

# Test case for the menu file
class TestMenu < Test::Unit::TestCase
  # Test that menu_item doesn't return a nil string
  def test_menu_item_not_null
    @item = Hash.new
    @item[:zer] = "aze"
    assert_not_nil(menu_item('aze', 'zer'))
  end

  # Check that the @item menu is set to "active"
  def test_menu_item_not_active
    @item = Hash.new
    @item[:menu] = "azening"
    lab = menu_item("Planning", "ppl/")
    assert_not_match /active/, lab
  end

  def test_menu_item_active
    @item = Hash.new
    @item[:menu] = "planning"
    lab = menu_item("Planning", "planning/")
    assert_match /active/, lab
  end
end
