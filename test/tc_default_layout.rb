require "test/unit"

DEFAULT='layouts/default.html'

# Test the content of the default layout file
class TestDefaultLayout < Test::Unit::TestCase
  # The default layout
  default_content=nil

  def setup
    @default_file   = File.open(DEFAULT)
    @default_content=@default_file.read
  end
  def cleanup
    @default_file.close()
  end
  
  # test that the default layout file copntains the given text
  def default_contains? text
    return @default_content.include? text
  end

  # Simply test the default layout file exists
  def test_default_exists
    assert_true(File.file? DEFAULT)
  end

  def test_website_name
    assert_true(default_contains? 'solebull.gitlab.io')
  end
  
  def test_project_link
    link='https://gitlab.com/solebull/solebull.gitlab.io'
    #    assert_true(default_contains? link)
  end
end
