# Simply run all unit tests

require 'test/unit'

require_relative "./tc_menu.rb"
require_relative "./tc_game_list.rb"
require_relative "./tc_links.rb"
require_relative "./tc_default_layout.rb"
