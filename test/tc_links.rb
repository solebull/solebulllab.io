require "test/unit"

require_relative "../lib/links.rb"

# Test case for the menu file
class TestLinks < Test::Unit::TestCase
  # Test that it only returns an empty string (not an assertion)
  # if the links YAML array is nil
  def test_links_null_array
    @item = Hash.new
    @item[:links] = nil
    assert_equal(links(), "")
  end

  # Test that the return table should be a div-based one
  def test_links_use_div
    @item = Hash.new
    @item[:links] = ["image.jpg", "link" "etc..."]
    assert_match(/div/, links())
  end

  # Assert that a one_link() function exists and doesn't return a nil string
  def test_links_one_link
    @item = Hash.new
    @item[:links] = ["image.jpg", "link" "etc..."]
    assert_not_nil(/div/, one_link(@item[:links][0]))
  end

  # Assert that one_link() function doesn't only return the first char
  def test_links_one_link_notOnlyFirstChar
    txt = one_link(["fulllink", "img"])
    assert_match(/fulllink/, txt)
  end

  # Link description should have the 'linkd' class
  def test_links_one_linkd
    txt = one_link(["fulllink", "img"])
    assert_match(/linkd/, txt)
  end

  # Test that the description field is processed through markdown parser
  # and, for example, replace *text* with <em>text</em>
  def test_descriptionhandle_markdown
    txt = one_link([ "twitch.png", "https://www.twitch.tv/solebull/",
                     "*text*"])
    assert_match(/<em>text<\/em>/, txt)
    
  end
end
