require "test/unit"

require "yaml" # Needed to call game_list outside nanoc environment

require_relative "../lib/game_list.rb"

# Test the game_list file's functions
class TestGameList < Test::Unit::TestCase
  # Tests the function that returns the printed name/link for a given game
  #
  # We should replace space chars with '-' with a non existing game
  # so not a link : we do not change 
  def test_game_slug_inexistant_game
    assert_equal("Red dead", game_slug("Red dead"))
  end

  # Tests the function with an existing game
  #
  # For a valid game name, search in src/content/game
  #
  # Pleast note : this test could fail if the game exist but the test are called
  # from the wrong root dir. Please call unit tests from src/.
  def test_game_slug_existant_game
    assert_equal("<a href='/game/the-forest/'>The forest</a>",
                 game_slug("The forest"))
  end

  # Test the game_exists function with an inexistant game
  def test_game_exists_no
    assert_equal(game_exists(game_slug("Inexistant game")), false)
  end

  # Test the game_exists function with an existant game
  def test_game_exists_ok
    slug = "the-forest"
    assert_equal(game_exists(slug), true)
  end

  # Test a randomly generated game name is not nil
  def test_random_game_name_nil
    n = random_game_name()
    assert_not_nil(n)
  end

  # Test a randomly generated game name is not empty
  def test_random_game_name_empty
    n = random_game_name()
    assert_not_equal(n, "")
  end

  # If game_list_cols is not defined in config, we have a nil early exit
  def test_game_list_nil_no_gameListCols
    # Fake configuration array
    @config = Hash.new
    @config[:many_games] = false
    
    gl = game_list()
    assert_nil(gl)
  end

  # Doesn't return an empty string
  def test_game_list_non_empty
    # Fake configuration array
    @config = Hash.new
    @config[:many_games] = false
    @config[:game_list_cols] = 3
    
    gl = game_list()
    assert(gl.size > 0)
  end

  # Returned string is a inner div
  def test_game_list_is_a_div
    # Fake configuration array
    @config = Hash.new
    @config[:many_games] = false
    @config[:game_list_cols] = 3
    
    gl = game_list()
    assert_includes(gl, 'div');
  end
  
end
