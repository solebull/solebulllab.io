.PHONY: clean

default: build #brave-verification

build:
	bundle exec nanoc

view:
	nanoc view

check:
	ruby test/tests.rb 2>/dev/null
# Same rule but with more verbose output
check-verbose:
	ruby test/tests.rb


doc:
	yard doc .
	yard stats . --list-undoc

# Install needed dependencies
deps:
	bundle install

clean:
	rm -fr doc

# Actually deactivated (not sure you have solebull.fr directory)
brave-verification:
	mkdir public/.well-known/
	cp ../solebull.fr/brave-rewards-verification.txt public/.well-known/
