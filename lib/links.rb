# A helper for homepage links

require 'stringio'
require 'kramdown'

require_relative './planning.rb'

## Return the text for one link
#
# \param item:: a table item from the page's YAML content :
#    [0] for the image name
#    [1] for the service link (twitch channel etc..)
#    [2] a markdown description
#
def one_link(item)
  # Process the descriprion part with the kramdown converter
  # We have to check parameter is not nil? to avoid a NoMethodError
  # due to a call of valid_encoding? on strng/nil
  md=item[2]?Kramdown::Document.new(item[2]).to_html : ""
  return "<div class='row'>
            <a href='#{item[1]}'>
            <div class='col-sm'><img class='service-logo' 
              src='img/#{item[0]}'></div>
            </a>
            <div class='col-sm linkd'>              
              <p>#{md}</p>           
            </div>
          </div>"
end

# We must return the new content
def links
  links = @item[:links]

  return "" if links.nil?
  

  io = StringIO.new
  links.each do |l|
    io << one_link(l)
  end

  ret = io.string
  return ret.encode("UTF-8")  
end
