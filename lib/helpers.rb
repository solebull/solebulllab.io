# Simply enable partials
# from https://nanoc.ws/doc/items-and-layouts/
use_helper Nanoc::Helpers::Rendering

include Nanoc::Helpers::XMLSitemap
