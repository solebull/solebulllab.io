# A helper to use a gamelist

# Translate a game name to a usable slug (URL part)
def game_slug(gamename)
  slug = gamename.downcase
  slug.gsub!(/\s/,'-')
  if not game_exists(slug) 
    warn "WARNING : content/game/" + slug + ".md not defined"
    return gamename
  else
    return "<a href='/game/#{slug}/'>#{gamename}</a>"
  end
end

# Does an individual markdown file exist for this game slug
#
# @param slug The game slug
#
# @return true if the file exist 
def game_exists(slug)
  file = Dir.pwd + "/content/game/" + slug + ".md"
  return File.exist?(file)
end

# Return a random game name for test purpose
#
# @return A string of a fictional game name
def random_game_name
  rl = "_ABCDEFGHIJKLMNOPQRSTUVWXYZ".chars.to_a

  named = ""
  10.times do
    named << rl.sample
  end
  return named.downcase
end

# Full game list
def game_list
  basedir = Dir.pwd + "/content/game/"
  gl = ""
  basename = []
  first_letter = ' '
  
  Dir.foreach(basedir) do |item|
    next if item == '.' or item == '..'
    next if item[-1] == '~'                 # Remove backup files
    basename.push(File.basename(item, '.md'))
  end

  if @config[:many_games]
    700.times do
      basename << random_game_name
    end
  end

  # Early exit if config is incomplete
  return nil if not @config[:game_list_cols]
  
  games_by_row = (basename.length / @config[:game_list_cols].to_i) + 1
  nb_games = 0
  gl = gl + '<div class="col-sm">'
  
  basename.sort.each do |item|
    filename = basedir + item + '.md'

      if @config[:many_games]
        if item[0] != first_letter then
          first_letter = item[0]
          gl = gl + "<h2>#{first_letter.capitalize}</h2>"
        end
      end

    if File.exist?(filename) then
    # do work on real items
      config = YAML.load_file(filename)
      name = config.fetch('title')
      gl = gl + "<a href='/game/#{item}'>#{name}</a><br>"
    else
      gl = gl + item + '<br>'
    end
    nb_games = nb_games + 1

    if nb_games > games_by_row then
      gl = gl + '</div><div class="col-sm">'
      gl = gl + "<h2>#{first_letter.capitalize} <small>(suite)</small></h2>"
      nb_games = 0
    end
      
  end
  gl = gl + '</div>'
  return gl
end
